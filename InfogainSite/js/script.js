// When the user scrolls the page, execute myFunction
window.onscroll = function() {
  myFunction();
};

// Get the navbar
var navbar = document.getElementById('careerValues');

// Get the offset position of the navbar
var sticky = navbar.offsetTop;

// Add the sticky class to the navbar when you reach its scroll position. Remove "sticky" when you leave the scroll position

function myFunction() {
  if ($(window).width() >= 768) {
    if (window.pageYOffset >= sticky) {
      navbar.classList.add('sticky');
    } else {
      navbar.classList.remove('sticky');
    }
  }
}

$('.career-value1').hover(
  function() {
    if ($(window).width() >= 960) {
      $('.career-value1>div>div>p').removeClass('text-666');
      $('.career-value1>div>div>h5').addClass('text-white');
      $('#animation1').attr('data', './img/icons-01_hover_animated.svg');
      // $('.career-value1>div>div>svg>image').attr('href', './img/marquee_client_white.png')
      $('.career-value-child1').addClass('career-value1-child1-animated');
    }
  },
  function() {
    if ($(window).width() >= 960) {
      $('.career-value1>div>div>p').addClass('text-666');
      $('.career-value1>div>div>h5').removeClass('text-white');
      $('#animation1').attr('data', './img/icons-01.svg');
      // $('.career-value1>div>div>svg>image').attr('href', './img/marquee_client.png')
      $('.career-value-child1').removeClass('career-value1-child1-animated');
    }
  }
);
$('.career-value2').hover(
  function() {
    if ($(window).width() >= 960) {
      $('.career-value2>div>div>p').removeClass('text-666');
      $('.career-value2>div>div>h5').addClass('text-white');
      $('#animation2').attr('data', './img/icons-02_hover_animated.svg');
      // $('.career-value2>div>div>svg>image').attr('href', './img/fast_growth_white.png')
      $('.career-value-child2').addClass('career-value1-child1-animated');
    }
  },
  function() {
    if ($(window).width() >= 960) {
      $('.career-value2>div>div>p').addClass('text-666');
      $('.career-value2>div>div>h5').removeClass('text-white');
      $('#animation2').attr('data', './img/icons-02.svg');
      // $('.career-value2>div>div>svg>image').attr('href', './img/fast_growth.png')
      $('.career-value-child2').removeClass('career-value1-child1-animated');
    }
  }
);
$('.career-value3').hover(
  function() {
    if ($(window).width() >= 960) {
      $('.career-value3>div>div>p').removeClass('text-666');
      $('.career-value3>div>div>h5').addClass('text-white');
      $('#animation3').attr('data', './img/icons-04_hover_animated.svg');
      // $('.career-value3>div>div>svg>image').attr('href', './img/empowering_people_white.png');
      $('.career-value-child3').addClass('career-value1-child1-animated');
    }
  },
  function() {
    if ($(window).width() >= 960) {
      $('.career-value3>div>div>p').addClass('text-666');
      $('.career-value3>div>div>h5').removeClass('text-white');
      $('#animation3').attr('data', './img/icons-04.svg');
      // $('.career-value3>div>div>svg>image').attr('href', './img/empowering_people.png');
      $('.career-value-child3').removeClass('career-value1-child1-animated');
    }
  }
);
$('.career-value4').hover(
  function() {
    if ($(window).width() >= 960) {
      $('.career-value4>div>div>p').removeClass('text-666');
      $('.career-value4>div>div>h5').addClass('text-white');
      $('#animation4').attr('data', './img/icons-03_hover_animated.svg');
      // $('.career-value4>div>div>svg>image').attr('href', './img/technology_white.png');
      $('.career-value-child4').addClass('career-value1-child1-animated');
    }
  },
  function() {
    if ($(window).width() >= 960) {
      $('.career-value4>div>div>p').addClass('text-666');
      $('.career-value4>div>div>h5').removeClass('text-white');
      $('#animation4').attr('data', './img/icons-03.svg');
      // $('.career-value4>div>div>svg>image').attr('href', './img/technologies.png');
      $('.career-value-child4').removeClass('career-value1-child1-animated');
    }
  }
);
